// Book Class : Represents Book

class Book {
    constructor(title, author, isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}

// UI Class : Handle UI Tasks

class UI {
    static displayBooks(){
        const StoredBooks = Store.getBooks();

        const books = StoredBooks;

        books.forEach(book => UI.addBookToList(book));
    }

    static addBookToList(book) {
        const list = document.querySelector('#book-list');


        const row = document.createElement('tr');
        
        row.innerHTML = `
        <td>${book.title}<td>
        <td>${book.author}<td>
        <td>${book.isbn}<td>
        <td><a href="#" class="btn btn-danger btn-sm delete">
        X
        <td>
        `;

        list.appendChild(row);
    }

    static removeBook(el){
        if (el.classList.contains('delete')) {
            el.parentElement.parentElement.remove();
        }
    }

    static clearFields(){
        document.querySelector('#title').value = '';
        document.querySelector('#author').value = '';
        document.querySelector('#isbn').value = '';
    }

    static showAlert(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        const container = document.querySelector('.container');
        const form = document.querySelector('#book-form');
        container.insertBefore(div, form);

        //Vanishing in 3 seconds
        setTimeout(
             () => document.querySelector('.alert').remove(), 
             3000
        );

    }

    static searchBook(text) {

        const books = document.querySelectorAll('#book-list tr');

        if(text === "") {
            console.log('vazio')
            books.forEach(book => {
                book.classList.remove('d-none');
            })
        }

        Array.from(books).forEach((book,index) => {
            const bookTitle = book.firstElementChild.textContent.toLowerCase();

            if (bookTitle.includes(text)) {
                // console.log('SIM');
                
            } else {
                // console.log('NAO');
                book.classList.add('d-none');
            }
        });

    }
}

// Storage Class : Handles Storage
class Store {

    static getBooks() {
        let books;
        if (localStorage.getItem('books') === null) {
            books = [];
        } else {
            books = JSON.parse(localStorage.getItem('books'));
        }

        return books;
    }

    static addBook(book) {
        const books = Store.getBooks();
        books.push(book);
        localStorage.setItem('books', JSON.stringify(books));
    }

    static removeBook(isbn) {
        const books = Store.getBooks();
        books.forEach((book, index)=>{
            if (book.isbn === isbn) {
                books.splice(index, 1);
            }
        })
        localStorage.setItem('books', JSON.stringify(books));
    }
}


// Event : Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks());

// Event : Add a Book
document.querySelector('#book-form').addEventListener('submit', e => {
    // Prevent actual submit
    e.preventDefault();

    // Get Form values
    const title = document.querySelector('#title').value;
    const author = document.querySelector('#author').value;
    const isbn = document.querySelector('#isbn').value;

    // Validate
    if (title === '' || author === '' || isbn === '') {
        UI.showAlert('Please filled all fields', 'danger');
    } else {
        // Instantiate Book
        const book = new Book(title, author, isbn);
        
        // Add book to list
        UI.addBookToList(book);

        // Add Book to Store
        Store.addBook(book);

        // Show success message
        UI.showAlert('Book Added', 'success');
        
        // Clear Fields
        UI.clearFields();
    }
    
    
})


// Event : Remove a Book
document.querySelector('#book-list').addEventListener('click',
 e => {
     // Remove Book from UI
     UI.deletBook(e.target);

    // Remove Book from Store
    Store.removeBook(e.target.parentElement.previousElementSibling.textContent);

    // Show success message
    UI.showAlert('Book Removed', 'success');
 }
)


// Event : Type in search

document.querySelector('#search').addEventListener('input', e => UI.searchBook(e.target.value.toLowerCase()));
